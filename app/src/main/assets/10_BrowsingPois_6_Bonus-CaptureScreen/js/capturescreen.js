/*
    Information about server communication. This sample webservice is provided by Wikitude and returns random dummy
    places near given location.
 */
var ServerInformation = {
    POIDATA_SERVER: "https://example.wikitude.com/GetSamplePois/",
    POIDATA_SERVER_ARG_LAT: "lat",
    POIDATA_SERVER_ARG_LON: "lon",
    POIDATA_SERVER_ARG_NR_POIS: "nrPois"
};

/* Implementation of AR-Experience (aka "World"). */
var World = {

    /*
        User's latest known location, accessible via userLocation.latitude, userLocation.longitude,
         userLocation.altitude.
     */
    userLocation: null,

    /* You may request new data from server periodically, however: in this sample data is only requested once. */
    isRequestingData: false,

    /* True once data was fetched. */
    initiallyLoadedData: false,

    /* True when world initialization is done. */
    initialized: false,

    /* Different POI-Marker assets. */
    markerDrawableIdle: null,
    markerDrawableSelected: null,
    markerDrawableDirectionIndicator: null,

    /* List of AR.GeoObjects that are currently shown in the scene / World. */
    markerList: [],

    /* the last selected marker. */
    currentMarker: null,

    locationUpdateCounter: 0,
    updatePlacemarkDistancesEveryXLocationUpdates: 10,

    /* Called to inject new POI data. */
    loadPoisFromJsonData: function loadPoisFromJsonDataFn(poiData) {

        /* Destroys all existing AR-Objects (markers & radar). */
        AR.context.destroyAll();

        /* Show radar & set click-listener. */
        PoiRadar.show();
        $('#radarContainer').unbind('click');
        $("#radarContainer").click(PoiRadar.clickedRadar);

        /* Empty list of visible markers. */
        World.markerList = [];

        /* Start loading marker assets. */
        World.markerDrawableIdle = new AR.ImageResource("assets/marker_idle.png", {
            onError: World.onError
        });
        World.markerDrawableSelected = new AR.ImageResource("assets/marker_selected.png", {
            onError: World.onError
        });
        World.markerDrawableDirectionIndicator = new AR.ImageResource("assets/indi.png", {
            onError: World.onError
        });

        /* Loop through POI-information and create an AR.GeoObject (=Marker) per POI. */
            var singlePoi = {
                "id": 1,
                "latitude": 20.654001,
                "longitude": -103.32584,
                "title": "Módulo A",
                "description": "En este módulo se encuentra la rectoría y las oficinas administrativas del CUCEI, además del auditorio Enrique Díaz de León"
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 2,
                "latitude": 20.654015,
                "longitude": -103.325049,
                "title": "Módulo B",
                "description": "En este módulo se imparten clases para los alumnos pertenecientes a la Licenciatura en Cultura Física."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 3,
                "latitude": 20.654241,
                "longitude": -103.325178,
                "title": "Módulo C",
                "description": "En este módulo se imparten clases para los alumnos pertenecientes a la Licenciatura en Cultura Física."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 4,
                "latitude": 20.654522,
                "longitude": -103.325478,
                "title": "Módulo D",
                "description": "Es un módulo ubicado atrás de la biblioteca, sólo contiene laboratorios de ciencias químicas. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 5,
                "latitude": 20.655561,
                "longitude": -103.325988,
                "title": "Módulo E",
                "description": "Módulo de dos pisos del área de ciencias químicas contiene 20 aulas y múltiples laboratorios."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 6,
                "latitude": 20.655797,
                "longitude": -103.325985,
                "title": "Módulo F",
                "description": "Es un módulo del área de ciencias químicas con 10 aulas de clases y un laboratorio."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 7,
                "latitude": 20.655842,
                "longitude": -103.326894,
                "title": "Módulo G",
                "description": "Módulo dedicado a ingeniería química que cuenta con dos aulas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 8,
                "latitude": 20.656055,
                "longitude": -103.326004,
                "title": "Módulo H",
                "description": "Módulo del área de ciencias químicas que solo contiene laboratorios."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 9,
                "latitude": 20.656056,
                "longitude": -103.32563,
                "title": "Módulo I",
                "description": "Es un módulo del área de ciencias químicas que sólo contiene 8 aulas de clases. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 10,
                "latitude": 20.656219,
                "longitude": -103.326035,
                "title": "Módulo J",
                "description": "Módulo con múltiples laboratorios y oficinas del área de ciencias químicas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 11,
                "latitude": 20.656365,
                "longitude": -103.326121,
                "title": "Módulo K",
                "description": "Es un pequeño módulo ubicado en el área de ciencias químicas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 12,
                "latitude": 20.656759,
                "longitude": -103.32521,
                "title": "Módulo L",
                "description": "Este módulo pertenece a la división de ciencias básicas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 13,
                "latitude": 20.656659,
                "longitude": -103.32617,
                "title": "Módulo M",
                "description": "En este modulo se encuentran los Laboratorios de Investigación."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 14,
                "latitude": 20.656963,
                "longitude": -103.326216,
                "title": "Módulo N",
                "description": "Este módulo pertenece a la división de electrónica y computación."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 15,
                "latitude": 20.657294,
                "longitude": -103.326254,
                "title": "Módulo O",
                "description": "Este módulo era la antigua biblioteca del CUCEI, actualmente en él se encuentran varios cubículos de maestros y coordinaciones de la división de electrónica y computación y división de ingenierías."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 16,
                "latitude": 20.657339,
                "longitude": -103.325412,
                "title": "Módulo P",
                "description": "Este módulo pertenece al departamento de ingeniería civil, tiene 23 aulas. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 17,
                "latitude": 20.6576,
                "longitude": -103.32516,
                "title": "Módulo Q",
                "description": "Este módulo pertenece al departamento de ingeniería industrial tiene 25 aulas y 3 laboratorios. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 18,
                "latitude": 20.65767,
                "longitude": -103.32568,
                "title": "Módulo R",
                "description": "Este módulo pertenece a los departamentos de ciencias computacionales y matemáticas, tiene 8 aulas y un laboratorio. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 19,
                "latitude": 20.657657,
                "longitude": -103.326314,
                "title": "Módulo S",
                "description": "El módulo S ubicado entre los módulos O y V cuenta con laboratorios de la Maestría en Ciencias en Ingeniería Electrónica y Computación, laboratorio de robótica, Site principal y oficinas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 20,
                "latitude": 20.657885,
                "longitude": -103.325526,
                "title": "Módulo T",
                "description": "Es un módulo del área de ciencias matemáticas. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 21,
                "latitude": 20.658101,
                "longitude": -103.325486,
                "title": "Módulo U",
                "description": "Módulo dedicado a matemáticas e ingeniería civil, cuenta con 14 aulas. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 22,
                "latitude": 20.658307,
                "longitude": -103.326344,
                "title": "Módulo V",
                "description": "Dedicado al departamento de matemáticas y física contiene las aulas de clase de estos departamentos, cuenta con 19 aulas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 23,
                "latitude": 20.658104,
                "longitude": -103.326249,
                "title": "Módulo V2",
                "description": ""
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 24,
                "latitude": 20.658078,
                "longitude": -103.326925,
                "title": "Módulo W",
                "description": "Este módulo, ubicado entre el CUCEI y la Prepa 12, está dedicado a computación y robótica, cuenta con 5 aulas. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 25,
                "latitude": 20.658253,
                "longitude": -103.326967,
                "title": "Módulo X",
                "description": "Dedicado a las Ciencias Computacionales, cuenta con 22 aulas."
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 26,
                "latitude": 20.657869,
                "longitude": -103.327549,
                "title": "Módulo Y",
                "description": "Es un módulo para exposiciones y laboratorios. Coloquialmente conocido como Chedraui"
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 27,
                "latitude": 20.658414,
                "longitude": -103.327674,
                "title": "Módulo Z",
                "description": "Módulo dedicado a las maestrías en física. "
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 28,
                "latitude": 20.656235,
                "longitude": -103.325223,
                "title": "Módulo Beta",
                "description": ""
            };
            World.markerList.push(new Marker(singlePoi));

            var singlePoi = {
                "id": 29,
                "latitude": 20.656467,
                "longitude": -103.325228,
                "title": "Módulo Alfa",
                "description": "En este módulo se imparten clases para los alumnos pertenecientes a la Licenciatura en Cultura Física."
            };
            World.markerList.push(new Marker(singlePoi));

        /* Updates distance information of all placemarks. */
        World.updateDistanceToUserValues();

        World.updateStatusMessage(currentPlaceNr + ' places loaded');

        /* Set distance slider to 100%. */
        $("#panel-distance-range").val(100);
        $("#panel-distance-range").slider("refresh");

        World.initialized = true;
    },

    /*
        Sets/updates distances of all makers so they are available way faster than calling (time-consuming)
        distanceToUser() method all the time.
     */
    updateDistanceToUserValues: function updateDistanceToUserValuesFn() {
        for (var i = 0; i < World.markerList.length; i++) {
            World.markerList[i].distanceToUser = World.markerList[i].markerObject.locations[0].distanceToUser();
        }
    },

    /* Updates status message shown in small "i"-button aligned bottom center. */
    updateStatusMessage: function updateStatusMessageFn(message, isWarning) {

        var themeToUse = isWarning ? "e" : "c";
        var iconToUse = isWarning ? "alert" : "info";

        $("#status-message").html(message);
        $("#popupInfoButton").buttonMarkup({
            theme: themeToUse,
            icon: iconToUse
        });
    },

    /*
        It may make sense to display POI details in your native style.
        In this sample a very simple native screen opens when user presses the 'More' button in HTML.
        This demoes the interaction between JavaScript and native code.
    */
    /* User clicked "More" button in POI-detail panel -> fire event to open native screen. */
    onPoiDetailMoreButtonClicked: function onPoiDetailMoreButtonClickedFn() {
        var nombre = World.currentMarker.poiData.title;
        nombre = nombre.replace(" ","");
                nombre = nombre.replace("ó","o");
        nombre = nombre.toLowerCase();
//        alert(nombre);
            AR.context.openInBrowser("http://arcucei.x10.mx/websites/" + nombre + ".html");
        var currentMarker = World.currentMarker;
        var markerSelectedJSON = {
            action: "present_poi_details",
            id: currentMarker.poiData.id,
            title: currentMarker.poiData.title,
            description: currentMarker.poiData.description
        };
        /*
            The sendJSONObject method can be used to send data from javascript to the native code.
        */
        AR.platform.sendJSONObject(markerSelectedJSON);
    },

    /* Location updates, fired every time you call architectView.setLocation() in native environment. */
    locationChanged: function locationChangedFn(lat, lon, alt, acc) {

        /* Store user's current location in World.userLocation, so you always know where user is. */
        World.userLocation = {
            'latitude': lat,
            'longitude': lon,
            'altitude': alt,
            'accuracy': acc
        };


        /* Request data if not already present. */
        if (!World.initiallyLoadedData) {
            World.requestDataFromServer(lat, lon);
            World.initiallyLoadedData = true;
        } else if (World.locationUpdateCounter === 0) {
            /*
                Update placemark distance information frequently, you max also update distances only every 10m with
                some more effort.
             */
            World.updateDistanceToUserValues();
        }

        /* Helper used to update placemark information every now and then (e.g. every 10 location upadtes fired). */
        World.locationUpdateCounter =
            (++World.locationUpdateCounter % World.updatePlacemarkDistancesEveryXLocationUpdates);
    },

    /*
        POIs usually have a name and sometimes a quite long description.
        Depending on your content type you may e.g. display a marker with its name and cropped description but
        allow the user to get more information after selecting it.
    */

    /* Fired when user pressed maker in cam. */
    onMarkerSelected: function onMarkerSelectedFn(marker) {
        World.currentMarker = marker;

        /*
            In this sample a POI detail panel appears when pressing a cam-marker (the blue box with title &
            description), compare index.html in the sample's directory.
        */
        /* Update panel values. */
        $("#poi-detail-title").html(marker.poiData.title);
        $("#poi-detail-description").html(marker.poiData.description);


        /*
            It's ok for AR.Location subclass objects to return a distance of `undefined`. In case such a distance
            was calculated when all distances were queried in `updateDistanceToUserValues`, we recalculate this
            specific distance before we update the UI.
         */
        if (undefined === marker.distanceToUser) {
            marker.distanceToUser = marker.markerObject.locations[0].distanceToUser();
        }

        /*
            Distance and altitude are measured in meters by the SDK. You may convert them to miles / feet if
            required.
        */
        var distanceToUserValue = (marker.distanceToUser > 999) ?
            ((marker.distanceToUser / 1000).toFixed(2) + " km") :
            (Math.round(marker.distanceToUser) + " m");

        $("#poi-detail-distance").html(distanceToUserValue);

        /* Show panel. */
        $("#panel-poidetail").panel("open", 123);

        $(".ui-panel-dismiss").unbind("mousedown");

        /* Deselect AR-marker when user exits detail screen div. */
        $("#panel-poidetail").on("panelbeforeclose", function(event, ui) {
            World.currentMarker.setDeselected(World.currentMarker);
        });
    },

    /* Screen was clicked but no geo-object was hit. */
    onScreenClick: function onScreenClickFn() {
        /* You may handle clicks on empty AR space too. */
    },

    /* Returns distance in meters of placemark with maxdistance * 1.1. */
    getMaxDistance: function getMaxDistanceFn() {

        /* Sort places by distance so the first entry is the one with the maximum distance. */
        World.markerList.sort(World.sortByDistanceSortingDescending);

        /* Use distanceToUser to get max-distance. */
        var maxDistanceMeters = World.markerList[0].distanceToUser;

        /*
            Return maximum distance times some factor >1.0 so ther is some room left and small movements of user
            don't cause places far away to disappear.
         */
        return maxDistanceMeters * 1.1;
    },

    /* Updates values show in "range panel". */
    updateRangeValues: function updateRangeValuesFn() {

        /* Get current slider value (0..100);. */
        var slider_value = $("#panel-distance-range").val();
        /* Max range relative to the maximum distance of all visible places. */
        var maxRangeMeters = Math.round(World.getMaxDistance() * (slider_value / 100));

        /* Range in meters including metric m/km. */
        var maxRangeValue = (maxRangeMeters > 999) ?
            ((maxRangeMeters / 1000).toFixed(2) + " km") :
            (Math.round(maxRangeMeters) + " m");

        /* Number of places within max-range. */
        var placesInRange = World.getNumberOfVisiblePlacesInRange(maxRangeMeters);

        /* Update UI labels accordingly. */
        $("#panel-distance-value").html(maxRangeValue);
        $("#panel-distance-places").html((placesInRange != 1) ?
            (placesInRange + " Places") : (placesInRange + " Place"));

        /* Update culling distance, so only places within given range are rendered. */
        AR.context.scene.cullingDistance = Math.max(maxRangeMeters, 1);

        /* Update radar's maxDistance so radius of radar is updated too. */
        PoiRadar.setMaxDistance(Math.max(maxRangeMeters, 1));
    },

    /* Returns number of places with same or lower distance than given range. */
    getNumberOfVisiblePlacesInRange: function getNumberOfVisiblePlacesInRangeFn(maxRangeMeters) {

        /* Sort markers by distance. */
        World.markerList.sort(World.sortByDistanceSorting);

        /* Loop through list and stop once a placemark is out of range ( -> very basic implementation ). */
        for (var i = 0; i < World.markerList.length; i++) {
            if (World.markerList[i].distanceToUser > maxRangeMeters) {
                return i;
            }
        }

        /* In case no placemark is out of range -> all are visible. */
        return World.markerList.length;
    },

    handlePanelMovements: function handlePanelMovementsFn() {

        $("#panel-distance").on("panelclose", function(event, ui) {
            $("#radarContainer").addClass("radarContainer_left");
            $("#radarContainer").removeClass("radarContainer_right");
            PoiRadar.updatePosition();
        });

        $("#panel-distance").on("panelopen", function(event, ui) {
            $("#radarContainer").removeClass("radarContainer_left");
            $("#radarContainer").addClass("radarContainer_right");
            PoiRadar.updatePosition();
        });
    },

    /* Display range slider. */
    showRange: function showRangeFn() {
        if (World.markerList.length > 0) {

            /* Update labels on every range movement. */
            $('#panel-distance-range').change(function() {
                World.updateRangeValues();
            });

            World.updateRangeValues();
            World.handlePanelMovements();

            /* Open panel. */
            $("#panel-distance").trigger("updatelayout");
            $("#panel-distance").panel("open", 1234);
        } else {

            /* No places are visible, because the are not loaded yet. */
            World.updateStatusMessage('No places available yet', true);
        }
    },

    /*
        This sample shows you how to use the function captureScreen to share a snapshot with your friends. Concept
        of interaction between JavaScript and native code is same as in the POI Detail sample.
        The "Snapshot"-button is on top right in the title bar. Once clicked the current screen is captured and
        user is prompted to share it (Handling of picture sharing is done in native code and cannot be done in
        JavaScript)
    */
    captureScreen: function captureScreenFn() {
        if (World.initialized) {
            AR.platform.sendJSONObject({
                action: "capture_screen"
            });
        }
    },

    /* Request POI data. */
    requestDataFromServer: function requestDataFromServerFn(lat, lon) {

        /* Set helper var to avoid requesting places while loading. */
        World.isRequestingData = true;
        World.updateStatusMessage('Requesting places from web-service');

        /* Server-url to JSON content provider. */
        var serverUrl = ServerInformation.POIDATA_SERVER + "?" + ServerInformation.POIDATA_SERVER_ARG_LAT + "=" +
            lat + "&" + ServerInformation.POIDATA_SERVER_ARG_LON + "=" +
            lon + "&" + ServerInformation.POIDATA_SERVER_ARG_NR_POIS + "=20";

        var jqxhr = $.getJSON(serverUrl, function(data) {
                World.loadPoisFromJsonData(data);
            })
            .error(function(err) {
                World.updateStatusMessage("Invalid web-service response.", true);
                World.isRequestingData = false;
            })
            .complete(function() {
                World.isRequestingData = false;
            });
    },

    /* Helper to sort places by distance. */
    sortByDistanceSorting: function sortByDistanceSortingFn(a, b) {
        return a.distanceToUser - b.distanceToUser;
    },

    /* Helper to sort places by distance, descending. */
    sortByDistanceSortingDescending: function sortByDistanceSortingDescendingFn(a, b) {
        return b.distanceToUser - a.distanceToUser;
    },

    onError: function onErrorFn(error) {
        alert(error)
    }
};


/* Forward locationChanges to custom function. */
AR.context.onLocationChanged = World.locationChanged;

/* Forward clicks in empty area to World. */
AR.context.onScreenClick = World.onScreenClick;