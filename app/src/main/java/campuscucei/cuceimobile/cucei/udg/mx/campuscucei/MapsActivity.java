package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import campuscucei.cuceimobile.cucei.udg.mx.campuscucei.SpellChecker.Spelling;

@SuppressWarnings("deprecation")
public class MapsActivity extends AppCompatActivity implements GoogleMap.OnInfoWindowClickListener, OnMapReadyCallback, LocationListener, Response.Listener<JSONObject>, Response.ErrorListener {

    //Variables globales que son usadas por diversas funciones y es mas facil declararlas globales para su acceso
    private GoogleMap mMap;
    private ProgressDialog progress;
    private JSONArray json;
    private JSONArray jsonEdificios;
    private JSONArray jsonCafeterias;
    private JSONArray jsonAuditorios;
    private JSONArray jsonServicios;
    private JSONArray jsonPapelerias;
    private Marker currentlyMarker;
    private ArrayList<Marker> listaMarcadores;
    //importante esta vista tiene que ser dejada como global ya que se accede mucho a ella y el crearla cada vez gasta memoria haciendo que la aplicacion explote
    private View vi;
    private AutoCompleteTextView buscador;
    private ImageButton menu;
    private LinearLayout menull;
    private CheckBox modulosCB, auditoriosCB, papeleriasCB, cafeteriasCB, serviciosCB;
    private TextView sugerencia;
    private String cadenaCorregida;
    //Preguntarle a Gris por esto.
    boolean menuE, modulosEnabled, auditoriosEnabled, cafeteriasEnabled, serviciosEnabled, papeleriasEnabled;
    boolean errorMesagge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        vi = getLayoutInflater().inflate(R.layout.infowindow_layout, null);
//        progress = new ProgressDialog(this);
        loadWebService();

        buscador = findViewById(R.id.buscador);
        menu = findViewById(R.id.menuBtn);
        menull = findViewById(R.id.menuEdificios);
        modulosCB = findViewById(R.id.modulos);
        auditoriosCB = findViewById(R.id.auditorios);
        papeleriasCB = findViewById(R.id.papelerias);
        cafeteriasCB = findViewById(R.id.cafeterias);
        serviciosCB = findViewById(R.id.servicios);
        sugerencia = findViewById(R.id.sugerencia);

        listaMarcadores = new ArrayList<>();
        menuE = false;

        String[] places = getResources().getStringArray(R.array.places);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.custom_list_item, R.id.text_view_list_item, places);
        buscador.setAdapter(adapter);

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        currentlyMarker = null;
        errorMesagge = false;
        //Esta linea en si es irrelevante pero android studio obliga a preguntar por los permisos aun cuando ya los tenga
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
        //El location manager se encarga de hacer que el listener actualice la ubicacion cada cierto tiempo
        LocationManager locationManager;
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 15000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 0, this);
        //Configuramos el mapa para que simplemente al abrir cargue en cucei y posea algunos items utiles como brujula
        LatLng cucei = new LatLng(20.657053600, -103.324953900);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cucei));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
        mMap.setOnInfoWindowClickListener(this);
        //mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        //Se añade un listener al mapa el cual sera el que trace la ruta de un punto a otro
        mMap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
            @Override
            public void onInfoWindowLongClick(Marker marker) {
                //variable usada para validaciones posteriores
                currentlyMarker = marker;
                //se limpia el mapa para que no haya mas rutas trazadas a la vez
                mMap.clear();
                addAllMarkers();
                //Se crea punto de origen y destino
                LatLng origin = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
                LatLng destiny = marker.getPosition();
                //se pasa al requesturl y se genera la ruta
                String url = getRequestUrl(origin, destiny);
                TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
                taskRequestDirections.execute(url);
                //Se oculta el infowindow para solo mostrar la ruta trazada
                marker.hideInfoWindow();
                //modificamos la camara para que apunte al origen de donde se encuentra ubicado actualmente y empezara a caminar
                mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(19);
                mMap.animateCamera(zoom);
            }
        });

        //Se crea un listener para las ventanas de informacion que se ejecutan al seleccionar un marcador
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            //Aqui se carga el contenido del infowindow
            @Override
            public View getInfoContents(Marker marker) {
                //objeto las referencias del titulo y la imagen que tendra
                TextView title = vi.findViewById(R.id.info_nombre);
                ImageView image = vi.findViewById(R.id.imagen_modulo);
                //La pongo en nulo ya que hacerlo directamente gasta mucha memoria y puede hacer que truene la app
                image.setImageDrawable(null);
                //cargo el nombre del edificio
                title.setText(marker.getTitle());
                //genero el nombre de la imagen
                String imageTitle = marker.getTitle();
                imageTitle = imageTitle.replace("ó", "o");
                imageTitle = imageTitle.replace("í", "i");
                imageTitle = imageTitle.replace("é", "e");
                imageTitle = imageTitle.replace("á", "a");
                imageTitle = imageTitle.replace(" ", "");
                imageTitle = imageTitle.toLowerCase();
                String uri = "@drawable/" + imageTitle;
                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                if (imageResource != 0) {
//                    Drawable imagen = ContextCompat.getDrawable(getApplicationContext(), imageResource);
                    image.setImageResource(imageResource);
                } else {
                    uri = "@drawable/auxiliarimage";
                    imageResource = getResources().getIdentifier(uri, null, getPackageName());
                    image.setImageResource(imageResource);
                }
                if (marker.getTitle().length() > 10) {
                    title.setTextSize(20);
                }
                return vi;
            }
        });

        //Un listener que al presionar en el mapa borra cualquier ruta trazada
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                currentlyMarker = null;
                mMap.clear();
                addAllMarkers();
            }
        });

        sugerencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscador.setText(cadenaCorregida);
                buscador.clearFocus();
                generarInferencia();
                sugerencia.setVisibility(View.GONE);
            }
        });

        //Listener del buscador
        buscador.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    sugerencia.setVisibility(View.GONE);
                    generarInferencia();
                    cadenaCorregida = "";
                    String cadenaOriginal = buscador.getText().toString().trim();
                    String[] arreglo = cadenaOriginal.split(" ");
                    for (int i = 0; i < arreglo.length; i++) {
                        cadenaCorregida += new Spelling().citySuggester(arreglo[i], getApplicationContext());
                        if (i + 1 < arreglo.length) {
                            cadenaCorregida += " ";
                        }
                    }
                    String cadenaAuxiliar1 = cadenaOriginal.toLowerCase();
                    cadenaAuxiliar1 = Normalizer.normalize(cadenaAuxiliar1, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                    String cadenaAuxiliar2 = cadenaCorregida.toLowerCase();
                    cadenaAuxiliar2 = Normalizer.normalize(cadenaAuxiliar2, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                    //Toast toast4 = Toast.makeText(getApplicationContext(), "Imprimo " + cadenaOriginal, Toast.LENGTH_SHORT);
                    //toast4.show();
                    if (!cadenaAuxiliar1.equals(cadenaAuxiliar2)) {
                        sugerencia.setVisibility(View.VISIBLE);
                        sugerencia.setText("¿Quizas quisiste decir \"" + cadenaCorregida + "\"?");
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
                return false;
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (menuE) {
                    menull.setVisibility(View.INVISIBLE);
                    menuE = false;
                } else {
                    menull.setVisibility(View.VISIBLE);
                    menuE = true;
                }
            }
        });

        modulosCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAllMarkers();
            }
        });

        auditoriosCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAllMarkers();
            }
        });

        papeleriasCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAllMarkers();
            }
        });

        serviciosCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAllMarkers();
            }
        });

        cafeteriasCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                addAllMarkers();
            }
        });

        permisosCamara();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            mMap.clear();
            addAllMarkers();
            currentlyMarker = null;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void generarInferencia() {
        String resultado;
        boolean encontrado = true;
        resultado = buscador.getText().toString();
        resultado = resultado.toLowerCase();
        resultado = Normalizer.normalize(resultado, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        if (!resultado.contains("como") && !resultado.contains("ruta") && !resultado.contains("llevame")) {
            for (int i = 0; i < listaMarcadores.size(); i++) {
                String markerTitle = listaMarcadores.get(i).getTitle();
                markerTitle = markerTitle.toLowerCase();
                markerTitle = Normalizer.normalize(markerTitle, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                if (resultado.contains(markerTitle)) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(listaMarcadores.get(i).getPosition()));
                    CameraUpdate zoomSearch = CameraUpdateFactory.zoomTo(19);
                    mMap.animateCamera(zoomSearch);
                    listaMarcadores.get(i).showInfoWindow();
                    encontrado = false;
                    break;
                }
            }
        }
        if (encontrado) {
            //Generaremos nuestra buscador inteligente que realizara las inferencias
            BuscadorInteligente inferencias = null;
            inferencias = new BuscadorInteligente(getApplicationContext(), buscador.getText().toString());
            resultado = inferencias.getResult();
            int casos;
            //Le aplicamos un filtro al resultado para saber cual es la intencion del usuario
            if (resultado.equals("donde_esta_algun_lugar")) {
                //si nos dice que esta buscando un lugar pero no sabe cual, pasamos la cadena original para que el sistema busque si existe un lugar asi
                resultado = buscador.getText().toString();
                casos = 1;
            } else if (resultado.equals("como_llegar_a_algun_lugar")) {
                //si nos dice que esta buscando un lugar pero no sabe cual, pasamos la cadena original para que el sistema busque si existe un lugar asi
                //resultado = buscador.getText().toString();
                casos = 2;
            } else if (resultado.contains("donde_esta_")) {
                //si nos dice directemente que busca un lugar lo pasamos directamente ya que la inferencia ya genero un lugar
                casos = 1;
            } else if (resultado.contains("como_llegar")) {
                //si quiere saber como llegar directamente lo enviamos debido a que la inferencia ya genero un lugar
                casos = 2;
            } else if (resultado.contains("ayuda")) {
                //en caso de ayuda el caso 3
                casos = 3;
            } else {
                //cualquier otro caso
                casos = 5;
            }

            //Toast toast4 = Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_SHORT);
            //toast4.show();
            //Parte de la correcion
/*                    Toast toast5 = Toast.makeText(getApplicationContext(), "La cadena corregida es: " + cadenaNueva, Toast.LENGTH_SHORT);
                    toast5.show();
                    toast5 = Toast.makeText(getApplicationContext(), "'" + cadenaNueva + "'" + " '" + cadenaOriginal + "'", Toast.LENGTH_SHORT);
                    toast5.show();
                    System.out.println("'" + cadenaNueva + "'" + " '" + buscador.getText().toString() + "'");*/
            //Fin de la correccion
//                    Toast toast4 = Toast.makeText(getApplicationContext(), "Imprimo" + resultado, Toast.LENGTH_SHORT);
//                    toast4.show();
            switch (casos) {
                case 1:
                    resultado = resultado.replace("donde_esta_", "");
                    resultado = resultado.replace("_", " ");
                    for (int i = 0; i < listaMarcadores.size(); i++) {
                        String markerTitle = listaMarcadores.get(i).getTitle();
                        markerTitle = markerTitle.toLowerCase();
                        markerTitle = Normalizer.normalize(markerTitle, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                        if (resultado.contains(markerTitle)) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(listaMarcadores.get(i).getPosition()));
                            CameraUpdate zoomSearch = CameraUpdateFactory.zoomTo(19);
                            mMap.animateCamera(zoomSearch);
                            listaMarcadores.get(i).showInfoWindow();
                            break;
                        }
                    }
                    break;
                case 2:
                    resultado = resultado.replace("como_llegar_", "");
                    resultado = resultado.replace("_", " ");
                    //se limpia el mapa para que no haya mas rutas trazadas a la vez
                    mMap.clear();
                    addAllMarkers();
                    //Buscamos el marcador indicado
                    for (int i = 0; i < listaMarcadores.size(); i++) {
                        String markerTitle = listaMarcadores.get(i).getTitle();
                        markerTitle = markerTitle.toLowerCase();
                        markerTitle = Normalizer.normalize(markerTitle, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                        if (resultado.contains(markerTitle)) {
                            currentlyMarker = listaMarcadores.get(i);
                            //Se crea punto de origen y destino
                            LatLng origin = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
                            LatLng destiny = currentlyMarker.getPosition();
                            //se pasa al requesturl y se genera la ruta
                            String url = getRequestUrl(origin, destiny);
                            TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
                            taskRequestDirections.execute(url);
                            //modificamos la camara para que
                            // apunte al origen de donde se encuentra ubicado actualmente y empezara a caminar
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
                            CameraUpdate zoom = CameraUpdateFactory.zoomTo(19);
                            mMap.animateCamera(zoom);
                            break;
                        }
                    }
                    break;
                case 3:
                    Intent abrir = new Intent(getApplicationContext(), InfoActivity.class);
                    startActivity(abrir);
                    break;
                default:
                    Toast toast3 = Toast.makeText(getApplicationContext(), "Lo sentimos el buscador, no pudo procesar tu solicitud.", Toast.LENGTH_SHORT);
                    toast3.show();
            }
        }
    }

    //Metodo que se ejecuta al presionar un infowindow y lo que hace es cargar una pagina con mas informacion sobre el modulo
    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent abrir = new Intent(getApplicationContext(), InfoEdificioActivity.class);
        abrir.putExtra("name", marker.getTitle());
        startActivity(abrir);
    }

    // Metodo usado para abrir el directorio
    public void abrirDirectorio(View v) {
        Intent abrir = new Intent(getApplicationContext(), DirectoryActivity.class);
        startActivity(abrir);
    }

    // Metodo usado para abrir el directorio
    public void abrirVideo(View v) {
        Intent abrir = new Intent(getApplicationContext(), VideoActivity.class);
        startActivity(abrir);
    }

    public void abrirInfo(View v) {
        Intent abrir = new Intent(getApplicationContext(), InfoActivity.class);
        startActivity(abrir);
    }

    // Metodo usado para abrir la camara
    public void abrirRealityAugment(View v) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.dialog_personal, null))
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        permisosCamara();
                        abrirRA();
                        dialog.cancel();
                    }
                }).show();
    }

    public void abrirRA() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Intent abrir = new Intent(getApplicationContext(), SimpleGeoArActivity.class);
            startActivity(abrir);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Error es necesario contar con Conexion a Internet.", Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(), "Error " + error.toString(), Toast.LENGTH_LONG).show();
//        progress.hide();
    }

    @Override
    public void onResponse(JSONObject response) {
        json = response.optJSONArray("markers");
        if (json != null && modulosCB.isChecked()) {
            jsonEdificios = json;
            addBuildings();
        }
        json = response.optJSONArray("cafeterias");
        if (json != null && cafeteriasCB.isChecked()) {
            jsonCafeterias = json;
            addCafeterias();
        }
        json = response.optJSONArray("auditorios");
        if (json != null && auditoriosCB.isChecked()) {
            jsonAuditorios = json;
            addAudtiorios();
        }
        json = response.optJSONArray("papelerias");
        if (json != null && papeleriasCB.isChecked()) {
            jsonPapelerias = json;
            addPapelerias();
        }
        json = response.optJSONArray("servicios");
        if (json != null && serviciosCB.isChecked()) {
            jsonServicios = json;
            addServicios();
        }
    }

    private void loadWebService() {
        //se crean las variables Json para la solicitud
        JsonObjectRequest request;
        RequestQueue mRequestQueue;
        mRequestQueue = Volley.newRequestQueue(this);
        //se carga un item de cargando para que el usuario espere
//        progress.setMessage("Cargando...");
//        progress.show();
        //se pasa la url del servidor y se hace la solicitud
        String url = "http://arcucei.x10.mx/querys/get_allmarkers.php";
        request = new JsonObjectRequest(Request.Method.POST, url, null, this, this);
        mRequestQueue.add(request);
    }

    public void addBuildings() {
        String name, description, latitude, longitude, dependencies, id;
        double lat, lon;
        int numberMarkers = 0;
        //Try catch para evitar que truene la aplicacion en caso de algun error
        try {
            numberMarkers = jsonEdificios.length();
            //bubcle que itera todos y cada uno de los marcadores
            for (int i = 0; i < numberMarkers; i++) {
                //inicializacion de una variable json y obtencion de los datos
                JSONObject jsonObject = null;
                jsonObject = jsonEdificios.getJSONObject(i);
                id = jsonObject.optString("id");
                name = jsonObject.optString("nombre");
                description = jsonObject.optString("descripcion");
                dependencies = jsonObject.optString("dependencias");
                latitude = jsonObject.optString("latitud");
                longitude = jsonObject.optString("longitud");
                lat = Double.parseDouble(latitude);
                lon = Double.parseDouble(longitude);
                LatLng edificio = new LatLng(lat, lon);
                //Creacion del marcador
                listaMarcadores.add(mMap.addMarker(new MarkerOptions()
                        .position(edificio)
                        .title(name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.edificio))));
            }
        } catch (Exception e) {
            e.printStackTrace();
            //    Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
            //            " " + e, Toast.LENGTH_LONG).show();
            errorMesagge = true;
        }
//        progress.hide();
    }

    public void addCafeterias() {
        String name, description, latitude, longitude, apertura, cierre, id;
        double lat, lon;
        int numberMarkers = 0;
        //Try catch para evitar que truene la aplicacion en caso de algun error
        try {
            numberMarkers = jsonCafeterias.length();
            //bubcle que itera todos y cada uno de los marcadores
            for (int i = 0; i < numberMarkers; i++) {
                //inicializacion de una variable json y obtencion de los datos
                JSONObject jsonObject = null;
                jsonObject = jsonCafeterias.getJSONObject(i);
                id = jsonObject.optString("id");
                name = jsonObject.optString("nombre");
                description = jsonObject.optString("descripcion");
                latitude = jsonObject.optString("latitud");
                longitude = jsonObject.optString("longitud");
                apertura = jsonObject.optString("hora_apertura");
                cierre = jsonObject.optString("hora_cierre");
                lat = Double.parseDouble(latitude);
                lon = Double.parseDouble(longitude);
                LatLng edificio = new LatLng(lat, lon);
                //Creacion del marcador
                listaMarcadores.add(mMap.addMarker(new MarkerOptions()
                        .position(edificio)
                        .title(name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.menu))));
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
//                    " " + e, Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor", Toast.LENGTH_LONG).show();
            errorMesagge = true;
        }
//        progress.hide();
    }

    public void addAudtiorios() {
        String name, description, latitude, longitude, id;
        double lat, lon;
        int numberMarkers = 0;
        //Try catch para evitar que truene la aplicacion en caso de algun error
        try {
            numberMarkers = jsonAuditorios.length();
            //bubcle que itera todos y cada uno de los marcadores
            for (int i = 0; i < numberMarkers; i++) {
                //inicializacion de una variable json y obtencion de los datos
                JSONObject jsonObject = null;
                jsonObject = jsonAuditorios.getJSONObject(i);
                id = jsonObject.optString("id");
                name = jsonObject.optString("nombre");
                description = jsonObject.optString("descripcion");
                latitude = jsonObject.optString("latitud");
                longitude = jsonObject.optString("longitud");
                lat = Double.parseDouble(latitude);
                lon = Double.parseDouble(longitude);
                LatLng edificio = new LatLng(lat, lon);
                //Creacion del marcador
                listaMarcadores.add(mMap.addMarker(new MarkerOptions()
                        .position(edificio)
                        .title(name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.audtiorio))));
            }
        } catch (Exception e) {
            e.printStackTrace();
            //          Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
            //                  " " + e, Toast.LENGTH_LONG).show();
            errorMesagge = true;
        }
//        progress.hide();
    }

    public void addPapelerias() {
        String name, description, latitude, longitude, apertura, cierre, id;
        double lat, lon;
        int numberMarkers = 0;
        //Try catch para evitar que truene la aplicacion en caso de algun error
        try {
            numberMarkers = jsonPapelerias.length();
            //bubcle que itera todos y cada uno de los marcadores
            for (int i = 0; i < numberMarkers; i++) {
                //inicializacion de una variable json y obtencion de los datos
                JSONObject jsonObject = null;
                jsonObject = jsonPapelerias.getJSONObject(i);
                id = jsonObject.optString("id");
                name = jsonObject.optString("nombre");
                description = jsonObject.optString("descripcion");
                latitude = jsonObject.optString("latitud");
                longitude = jsonObject.optString("longitud");
                apertura = jsonObject.optString("hora_apertura");
                cierre = jsonObject.optString("hora_cierre");
                lat = Double.parseDouble(latitude);
                lon = Double.parseDouble(longitude);
                LatLng edificio = new LatLng(lat, lon);
                //Creacion del marcador
                listaMarcadores.add(mMap.addMarker(new MarkerOptions()
                        .position(edificio)
                        .title(name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.papeleria))));
            }
        } catch (Exception e) {
            e.printStackTrace();
            //   Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
            //           " " + e, Toast.LENGTH_LONG).show();
            errorMesagge = true;
        }
//        progress.hide();
    }

    public void addServicios() {
        String name, description, latitude, longitude, apertura, cierre, id;
        double lat, lon;
        int numberMarkers = 0;
        //Try catch para evitar que truene la aplicacion en caso de algun error
        try {
            numberMarkers = jsonServicios.length();
            //bubcle que itera todos y cada uno de los marcadores
            for (int i = 0; i < numberMarkers; i++) {
                //inicializacion de una variable json y obtencion de los datos
                JSONObject jsonObject = null;
                jsonObject = jsonServicios.getJSONObject(i);
                id = jsonObject.optString("id");
                name = jsonObject.optString("nombre");
                description = jsonObject.optString("descripcion");
                latitude = jsonObject.optString("latitud");
                longitude = jsonObject.optString("longitud");
                apertura = jsonObject.optString("hora_apertura");
                cierre = jsonObject.optString("hora_cierre");
                lat = Double.parseDouble(latitude);
                lon = Double.parseDouble(longitude);
                LatLng edificio = new LatLng(lat, lon);
                //Creacion del marcador
                listaMarcadores.add(mMap.addMarker(new MarkerOptions()
                        .position(edificio)
                        .title(name)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.services))));
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
            //        " " + e, Toast.LENGTH_LONG).show();
            errorMesagge = true;
        }
//        progress.hide();
    }

    //Este metodo se encarga de limpiar la lista de marcadores y los marcadores en el mapa para solo cargar los solicitados.
    public void addAllMarkers() {
        listaMarcadores.clear();
        mMap.clear();
        if (modulosCB.isChecked()) {
            addBuildings();
        }
        if (auditoriosCB.isChecked()) {
            addAudtiorios();
        }
        if (cafeteriasCB.isChecked()) {
            addCafeterias();
        }
        if (papeleriasCB.isChecked()) {
            addPapelerias();
        }
        if (serviciosCB.isChecked()) {
            addServicios();
        }
        if (errorMesagge) {
            Toast.makeText(getApplicationContext(), "Error de Conexion, vuelva a intentarlo en unos minutos.", Toast.LENGTH_LONG).show();
            errorMesagge = false;
        }
    }


    //En este metodo se configura lo necesario para generar una url que nos dara la ruta de un punto a otro
    private String getRequestUrl(LatLng origin, LatLng dest) {
        //Value of origin
        String str_org = "origin=" + origin.latitude + "," + origin.longitude;
        //Value of destination
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        //Set value enable the sensor
        String sensor = "sensor=false";
        //Mode for find direction
        String mode = "mode=walking";
        //Build the full param
        String param = str_org + "&" + str_dest + "&" + sensor + "&" + mode;
        //Output format
        String output = "json";
        //Create url to request
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" +
                param + "&key=AIzaSyBWBbf1OcAz-lG22nA-2IMumQDfbzo4HT0";
        return url;
    }

    private String requestDirection(String reqUrl) throws IOException {
        String responseString = "";
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(reqUrl);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            //Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer = new StringBuffer();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            responseString = stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            httpURLConnection.disconnect();
        }
        return responseString;
    }

    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String responseString = null;
            try {
                responseString = requestDirection(strings[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Parse json here
            TaskParser taskParser = new TaskParser();
            taskParser.execute(s);
        }
    }

    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {
        PatternItem DOT = new Dot();
        PatternItem DASH = new Dash(25);
        List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(DOT);

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject = null;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jsonObject = new JSONObject(strings[0]);
                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            //Get list route and display it into the map

            ArrayList points = null;

            PolylineOptions polylineOptions = null;

            for (List<HashMap<String, String>> path : lists) {
                // Toast.makeText(getApplicationContext(), "I aM hERE!", Toast.LENGTH_SHORT).show();
                points = new ArrayList();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    double lat = Double.parseDouble(point.get("lat"));
                    double lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat, lon));
                }

                polylineOptions.addAll(points);
                polylineOptions.width(30);
                polylineOptions.color(Color.argb(100, 12, 116, 196));
                polylineOptions.startCap(new RoundCap());
                polylineOptions.pattern(PATTERN_POLYGON_ALPHA);
                polylineOptions.geodesic(true);
            }

            if (polylineOptions != null) {
                mMap.addPolyline(polylineOptions);
            } else {
                Toast.makeText(getApplicationContext(), "Direction not found!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    //Metodo utilizado para cuando cambie la ubicacion actualice el lugar en donde estas y como llegar al aula
    @Override
    public void onLocationChanged(Location location) {
        //validacion para evitar que el marcador sea nulo o que la app se ejecute en segundo plano y genere errores
        if (currentlyMarker != null && location != null) {
            mMap.clear();
            addAllMarkers();
            LatLng origin = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
            LatLng destiny = currentlyMarker.getPosition();
            //Create the URL to get request from first marker to second marker
            String url = getRequestUrl(origin, destiny);
            //Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT).show();
            TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
            taskRequestDirections.execute(url);
        }
    }

    //a partir de aqui los metodos no son usados pero es necesario tenerlos debido a que se añadio un listener a la clase.
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void permisosCamara() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
                // MY_PERMISSIONS_REQUEST_CAMARA es una constante definida en la app. El método callback obtiene el resultado de la petición.

            }
        } else { //have permissions
        }
    }

}
