package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

//activity que carga el video de la app
public class VideoActivity extends Activity {

    WebView wv;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videocucei);
        wv = findViewById(R.id.videoView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/video.html");
        wv.setWebViewClient(new WebViewClient(){
            public  boolean shouldOverrideUrlLoading (WebView view, String url){
                return false;
            }
        });
    }

}
