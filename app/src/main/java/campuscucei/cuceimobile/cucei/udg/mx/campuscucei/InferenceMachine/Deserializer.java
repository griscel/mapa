package campuscucei.cuceimobile.cucei.udg.mx.campuscucei.InferenceMachine;


import android.util.Log;

import java.util.ArrayList;

public class Deserializer
{
    private String fileString;

    public Deserializer(String fileString)
    {
        this.fileString = fileString;
    }

    public ArrayList<Rule> deserialize()
    {
        /*tomar la regla como renglon*/
        String [] lines = fileString.split("\n");
        ArrayList<Rule> kb = new ArrayList<>();

        for (String line: lines)
        {
            //Log.i("linea", line);

            if (!line.equals("<bc>") &&
                    !line.equals("</bc>"))
            {
                Rule tmpRule;
                tmpRule = readRegla(line);
                if (tmpRule == null)
                {
                    Log.i("error","Wrong Format");
                    return null;
                }
                else
                    kb.add(tmpRule);
            }

        }
        return kb;
    }

    private Rule readRegla(String line)
    {
        /*Analizar regla por partes*/
        Rule newRule = new Rule();

        String [] words = line.split(" ");
        boolean conditions,conclusions,atomo,objetive;

        conditions=conclusions=atomo=objetive=false;

        if(!words[0].equals("<regla>"))
            return null;

        for (String word:words)
        {
            switch (word)
            {
                case "<atomo>":
                    atomo = true;
                    objetive = false;
                    break;
                case "</atomo>":
                    atomo = false;
                    objetive = false;
                    break;
                case "<atomoObj>":
                    atomo = true;
                    objetive = true;
                    newRule.setObjective(true);
                    break;
                case "</atomoObj>":
                    atomo = false;
                    objetive = false;
                    break;
                case "<condicion>":
                    conditions = true;
                    break;
                case "</condicion>":
                    conditions = false;
                    break;
                case "<conclusion>":
                    conclusions = true;
                    break;
                case "</conclusion>":
                    conclusions = false;
                    break;
                case "<negacion/>":
                    if (conditions && !conclusions)
                    {
                        newRule.addToConditions("~");
                    }
                    if (conclusions && !conditions)
                    {
                        newRule.addToConclusions("~");
                    }
                    break;
                case "<conjuncion/>":
                    if (conditions && !conclusions)
                    {
                        newRule.addToConditions("&");
                    }
                    if (conclusions && !conditions)
                    {
                        newRule.addToConclusions("&");
                    }
                    break;
                case "<disyuncion/>":
                    if (conditions && !conclusions)
                    {
                        newRule.addToConditions("|");
                    }
                    if (conclusions && !conditions)
                    {
                        newRule.addToConclusions("|");
                    }
                    break;
                default:
                    if (atomo)
                    {
                        Atom newAtomo = new Atom(word, true, objetive);
                        if (conditions && !conclusions)
                        {
                            newRule.addToConditions(newAtomo);
                        }
                        if (conclusions && !conditions)
                        {
                            newRule.addToConclusions(newAtomo);
                        }
                    }
                    break;
            }
        }
        return newRule;
    }
}