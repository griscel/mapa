package campuscucei.cuceimobile.cucei.udg.mx.campuscucei.InferenceMachine;
import java.util.ArrayList;

public class Rule
{
    private ArrayList<Object> conditions;
    private ArrayList<Object> conclusions;
    private boolean met, objective, usedAsObjective, used;


    Rule()
    {
        conditions = new ArrayList<>();
        conclusions = new ArrayList<>();
        usedAsObjective = false;
        used = false;
        objective = false;
    }

    public void setObjective(boolean objective)
    {
        this.objective = objective;
    }

    public boolean isObjective()
    {
        return objective;
    }

    public void addToConditions(Object newCondition)
    {
        conditions.add(newCondition);
    }

    public void addToConclusions(Object newConclusion)
    {
        conclusions.add(newConclusion);
    }

    public ArrayList<Object> getConditions()
    {
        return conditions;
    }

    public ArrayList<Object> getConclusions()
    {
        return conclusions;
    }

    public void setMet(boolean met)
    {
        this.met = met;
    }

    public boolean isMet()
    {
        return met;
    }

    @Override
    public String toString()
    {
        StringBuilder ruleDescription= new StringBuilder("SI ");
        for(Object condition : conditions)
        {
            ruleDescription.append(condition).append(" ");
        }
        ruleDescription.append("ENTONCES ");

        for(Object conclusion : conclusions)
        {
            ruleDescription.append(conclusion).append(" ");
        }
        return ruleDescription.toString();
    }

    public boolean isUsedAsObjective()
    {
        return usedAsObjective;
    }

    public void setUsedAsObjective(boolean usedAsObjective)
    {
        this.usedAsObjective = usedAsObjective;
    }

    public void setUsed(boolean used)
    {
        this.used = used;
    }

    public boolean isUsed()
    {
        return used;
    }
}
