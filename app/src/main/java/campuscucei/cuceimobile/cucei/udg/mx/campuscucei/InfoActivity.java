package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class InfoActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_activity);
    }

    public void abrirTutorial(View v){
        Intent abrir = new Intent(getApplicationContext(), TutorialActivity.class);
        startActivity(abrir);
    }

    public void abrirQuejas(View v){
        Intent abrir = new Intent(getApplicationContext(), QuejasActivity.class);
        startActivity(abrir);
    }
}
