package campuscucei.cuceimobile.cucei.udg.mx.campuscucei.SpellChecker;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Spelling {

    //Nombre del archivo que sera nuestro diccionario
    String wordsfile = "PalabrasComunes.txt";
    //un Arayy  de strings para almacenar las cadenas que estan en nuestro archivo de diccionario
    ArrayList<String> wordslist = new ArrayList<String>();
    //Un hashmap por que pus si
    HashMap<String, Integer> newlist = new HashMap<String, Integer>();
    //una varaible auxiliar solo para tener guardada la cadena original
    String original = "";

    public Spelling() {
        original = "";
        newlist = new HashMap<String, Integer>();
        wordsfile = "PalabrasComunes.txt";
    }

    public void loadcities(String filename, Context contexto) throws IOException {
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(contexto.getAssets().open(filename), "ISO-8859-1"));
        while ((line = br.readLine()) != null) {
            //Una validacion para evitar problemas solo tomaremos palabras mayores a 3 letras y menores a 26
            if (line.length() > 2 && line.length() < 26) {
                //Se añade a la lista de palabras
                wordslist.add(line);
            }
        }
    }

    public String citySuggester(String cityName, Context contexto) {
        int tamanio = cityName.length() / 2;
        //Variable auxiliar que usaremos despues
        boolean cadenaNoEncontrada = true;
        String result = "";
        original = cityName;

        int i;
        //Se carga el archivo para empezar a utilizar las cadenas ya contenidas alli
        try {
            loadcities(wordsfile, contexto);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        //Se itera la lista de cadenas
        for (String s : wordslist) {
            //Se obtiene la distancia de leveinstain de cada una de ellas
            i = LevenshteinDistance.computeDistance(s, cityName);
            //Se comprueba si la distancia entre palabras es menor a 3
            if (i <= tamanio) {
                //si la cadena es menor a 3 si se añade a la lista de posibles respuestas
                newlist.put(s, i);
            }
        }

        //Creo una lista de entradas
        List<Entry<String, Integer>> entries = new ArrayList<Entry<String, Integer>>(newlist.entrySet());
        //Genero mi metodo Sort para ordenar mi lista de candidatos, y este metodo de ordenamiento
        //compara 2 cadenas y regresa la que tenga un menor valor de diferencia mediante -1 0 1
        Collections.sort(entries, new Comparator<Entry<String, Integer>>() {
            public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
                return e1.getValue().compareTo(e2.getValue());
            }
        });

        //Meto mis valores a una nueva lista por asi decirlo
        Map<String, Integer> orderedMap = new LinkedHashMap<String, Integer>();
        for (Entry<String, Integer> entry : entries) {
            orderedMap.put(entry.getKey(), entry.getValue());
        }

        //Itero mi lista de resultados que deberia estar ordenado de la mas cercana a la mas lejana
        for (Entry<String, Integer> e : orderedMap.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
            cadenaNoEncontrada = false;
            //Aqui dijo que mi resultado sera la cadena mas cercana, que en el mayor de los casos sera una
            result = e.getKey();
            break;
        }

        //Si la cadena no encontro ninguna retorno el resultado
        if (cadenaNoEncontrada) {
//            System.out.println("Imprimo " + original);
            result = original;
        }
        return result;
    }

}
