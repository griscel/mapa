package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class DirectoryActivity extends Activity {

    WebView webView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.directory);
        webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/directorio.html");
        webView.setWebViewClient(new WebViewClient(){
            public  boolean shouldOverrideUrlLoading (WebView view, String url){
                return false;
            }
        });
    }
}
