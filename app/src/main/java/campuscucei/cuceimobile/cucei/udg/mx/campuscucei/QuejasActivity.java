package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;




import android.os.Bundle;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class QuejasActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<String> {

    StringRequest request;
    RequestQueue mRequestQueue;
    private EditText asunto,mensaje;
    String sub="",mes="",url="";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quejas_activity);

        asunto = findViewById(R.id.asunto);
        mensaje = findViewById(R.id.mensaje);
        mRequestQueue = Volley.newRequestQueue(this);

    }

    public void agregarQueja(View v){
        sub=asunto.getText().toString();
        mes=mensaje.getText().toString();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String fecha = dateFormat.format(date);
        if(!sub.equals("")&&!mes.equals("")){
            url="http://arcucei.x10.mx/querys/add_message.php?asunto="+sub+"&mensaje="+mes+"&fecha="+fecha;
            url=url.replace(" ","%20");
            request = new StringRequest(Request.Method.GET,url,this,this);
            mRequestQueue.add(request);
            asunto.setText("");
            mensaje.setText("");

        }
        else{
            Toast.makeText(getApplicationContext(),"Se requieren ambos campos",Toast.LENGTH_SHORT).show();
        }
    }

    public void onErrorResponse(VolleyError volleyError) {
        Toast.makeText(getApplicationContext(),"Error: "+volleyError,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String response) {
        Toast.makeText(getApplicationContext(),"Mensaje enviado "+response,Toast.LENGTH_SHORT).show();
        this.onBackPressed();
    }

}
