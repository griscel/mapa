package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class InfoEdificioActivity extends Activity {

    WebView webView;
    TextView tv;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Se recibe la variable string que se envio a esta clase
        String name = getIntent().getExtras().getString("name");
        String titleShow = getIntent().getExtras().getString("name");
        //Se realiza unas modificaciones para poder utilizar el nomnbre del modulo como nombre del archivo html a cargar
        name = name.replace("ó", "o");
        name = name.replace("í", "i");
        name = name.replace("é", "e");
        name = name.replace("á", "a");
        name = name.replace(" ", "");
        name = name.toLowerCase();
        //Se atrapan las referencias de los elementos a utilizar
        setContentView(R.layout.edificio);
        tv=findViewById(R.id.nombreE);
        tv.setText(titleShow);
        webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://arcucei.x10.mx/websites/" + name + ".html");
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
    }
}

