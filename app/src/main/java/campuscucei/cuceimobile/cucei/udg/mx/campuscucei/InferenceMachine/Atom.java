package campuscucei.cuceimobile.cucei.udg.mx.campuscucei.InferenceMachine;


public class Atom
{
    private String content;
    private boolean state, objective;

    Atom(String content, boolean state, boolean objective)
    {
        this.content = content.toLowerCase();
        this.state = state;
        this.objective = objective;
    }

    public boolean getState()
    {
        return state;
    }

    public boolean isObjective()
    {
        return objective;
    }

    public String getContent()
    {
        return content;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public String toString()
    {
        return (state ? "" : "~") + (objective ? "->" : "" ) + content;
    }

    @Override
    public boolean equals(Object obj)
    {
        Atom aux = (Atom) obj;
        return content.equals(aux.content) && (state == aux.getState());
    }
}
