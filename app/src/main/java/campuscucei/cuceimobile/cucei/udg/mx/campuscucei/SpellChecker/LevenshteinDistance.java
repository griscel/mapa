package campuscucei.cuceimobile.cucei.udg.mx.campuscucei.SpellChecker;

//Clase creada para obtener la distancia de levenstain
public class LevenshteinDistance {

    //Este metodo se encarga de obtener la diferencia entre una palabra y otras
    public static int computeDistance(String s1, String s2) {
        //Para evitar problemas debido a comprobaciones posibles, solo podemos corregir palabras que ya esten terminadas es decir no podemos completar palabras que esten a la mitad
        //ya que si lo hicieramos habria casos donde una palabra completa podria ser corregida por otra palabra
        if (s1.length() != s2.length()) {
            //retornamos un numero grande para indicar que no es un candidato la palabra que tenemos
            return 20;
        }
        //usamos to lowercase para pasarlas a minusculas
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        // declaro un arreglo de enteros
        int[] costs = new int[s2.length() + 1];
        //ciclo for para iterar sobre mi cadena 1, de esa manera iterare siempre la cadena sacada del diccionario
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            //iteracion sobre mi cadena
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0) {
                    costs[j] = j;
                } else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
                            newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                        }
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0) {
                costs[s2.length()] = lastValue;
            }
        }
        //Regresamos la distancia obtenida
        return costs[s2.length()];
    }
}
