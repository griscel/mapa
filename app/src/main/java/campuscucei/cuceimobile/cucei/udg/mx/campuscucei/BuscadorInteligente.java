package campuscucei.cuceimobile.cucei.udg.mx.campuscucei;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;

import campuscucei.cuceimobile.cucei.udg.mx.campuscucei.InferenceMachine.*;

public class BuscadorInteligente {
    //Variables para saber si hacer encadenamiento hacia adelante o hacia atras, en nuestro caso el encadenamiento hacia adelante ofrece un mejor funcioamiento
    //basado en las reglas de inferencia para nuestra app
    boolean forward, backward;
    //nombre para el archivo
    String fileString;
    //herramienta que utilizaremos para leer el xml
    FileReader fReader;
    //Nuestra clase que se encargara de interpretar el contenido del xml en reglas para nuestra maquina de inferencia
    Deserializer deserializer;
    //Base de conocimientos
    KnowledgeBase kb;
    //motor de inferencia
    InferenceEngine ie;
    //variable que utilizaremos para saber cuando detener el ciclo continuo de inferencias a nuestra cadena
    int inferenceRunning;
    Context context;
    String result;

    public BuscadorInteligente(Context contextx, String cadenax) {
        result = "";
        context = contextx;
        //Aqui especificamos que usaremos unicamente el encadenamiento hacia adelante
        forward = true;
        backward = false;
        //Nuestra variable la inicializamos en 1 para indicar que nuestra maquina de inferencia ya esta corriendo o esta aun corriendo
        inferenceRunning = 1;
        //Creamos nuestro objeto lector, freader
        fReader = new FileReader();
        //Cargamos el archivo con nuestras reglas
        try {
            load(getFileFromAssets());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //inicializamos el encadenamiento hacia adelante
        beginForward();

        //Ciclo while en el que se hacen comparaciones con el string introducido con el fin de hacer inferencias hasta llegar a una conclusion
        while (inferenceRunning == 1) {
            //aqui usamos cadenas axuliares, en especifico para tratar la informacion ya que lo que introduzca el usuario debe ser cambiado a una manera
            //que nuestra maquina de inferencia sea capaz de entender lo que se le esta introducion, y capaz de contestar la pregunta de si o no
            String cadena = cadenax;
            cadena = cadena.toLowerCase();
            cadena = Normalizer.normalize(cadena, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
            cadena = cadena.replace(" ", "_");
            String pregunta = ie.getCurrentAtom();
            //aqui hacemos la comparacion si la cadena contiene lo de la pregunta, eso significa que el usuario si esta preguntando lo mismo que la maquina
            //de inferencia esta deduciendo, de otra manera se introduce un false y la maquina de inferencia buscara la suficiente informacion como para
            //llegar a algun resultado
            if (cadena.contains(pregunta)) {
                ie.setCurrentAtomState(true);
            } else {
                ie.setCurrentAtomState(false);
            }
            //response es la respuesta a lo que tenemos actualmente y forwardchaining simplemente se actualiza de regla a la siguiente si la hay
            ArrayList response = ie.forwardchaining();
            //se comprueba que al respuesta nos hizo llegar a algo o si nos dio un error
            checkForFinale(response);
        }

    }

    //este metodo obtiene la referencia de donde esta nuestro xml
    public BufferedReader getFileFromAssets() throws IOException {
        //Se carga el archivo con el nombre designado
        return new BufferedReader(new InputStreamReader(context.getAssets().open("conocimiento.xml")));
    }

    //este metodo interpreta y carga el archivo
    public void load(BufferedReader newFile) throws IOException {
        //el archivo cargado es recibido y se asigna a una variable filestring
        fileString = fReader.readFile(newFile);
        //se crea nuestro objeto deserializador
        deserializer = new Deserializer(fileString);
        //lo que nos regresara el objeto deselizador sera el archivo leido e interpretado en reglas, regresndonos un array de reglas
        //las cuales seran pasados a nuestra base de conocimiento para tenerlas disponibles
        kb = new KnowledgeBase(deserializer.deserialize());
        //Si la base de conocimientos esta vacia no corre el sistema
        if (!kb.isEmpty()) {
            //de otra manera la base de conocimiento es pasado al motor de inferencia
            ie = new InferenceEngine(kb);
            //este metodo se utiliza solo si se escogio el encadenamiento hacia atras, y funciona de tal manera que primero se tiene que ejecutar ya que el
            //encadenamiento hacia adelante depende del encadenamiento hacia atras
            if (backward) {
                ie.backwardChaining();
            }
            ie.forwardchaining();
        }
    }

    //este metodo se encarga de comprobar si ya la inferencia ha terminado o sigue corriendo
    void checkForFinale(ArrayList response) {
        //si respuesta es mayor a cero significa que hemos llegado a algo
        if (response.size() > 0) {
            String responseString = String.valueOf(response);
            int x = (response.get(0) == "Failed" ? 1 : 3);
            responseString = responseString.substring(x, responseString.length() - 1);

            if (responseString.equals("Failed")) {
                if (backward) {
                    Log.d("PBackward", "response" + responseString);
                    ie.clearSuccess();
                    if (!ie.backwardChaining()) {
                        Log.d("PBackward", "False");
                        finishProcess(responseString);
                    } else {
                        Log.d("PBackward", "True");
                        ArrayList _response = ie.forwardchaining();

                        checkForFinale(_response);
                    }
                } else {
                    finishProcess(responseString);
                }
            } else {
                finishProcess(responseString);
            }
        }
    }

    //este metodo es utilizado unicamente para anuncion que hemos llegado o a un fallo o a una conclusion
    void finishProcess(String response) {
        if (response.equals("Failed")) {
            //            result.setTextColor(ContextCompat.getColor(context, R.color.colorError));
            //Aqui podemos introducir determinado codigo
        } else {
            //aqui tambien
        }
/*        Toast toast1 = Toast.makeText(context, "Conclusion: " + response, Toast.LENGTH_SHORT);
        toast1.show();*/
        //cambiamos el valor de esta variable para indicar que la maquina ya llego a algo y se puede detener ya
        inferenceRunning = 0;
        result = response;
    }

    //metodo usado solo para inicializar la maquina
    void beginForward() {
        inferenceRunning = 1;
        //inicializamos las cosas que ocupamos para nuestra maquina de inferencia
        ie = new InferenceEngine(ie.getKb());
        ie.forwardchaining();
    }

    public String getResult() {
        return result;
    }

    /*    public String cadena;
    public ArrayList<Marker> markerList;
    public int markerID;

    public BuscadorInteligente(String cadenax, ArrayList<Marker> lista) {
        cadena = cadenax;
        markerList = new ArrayList<>();
        markerList = lista;
    }

    public int generarInferencia() {
        int resultado = 0;
        cadena = cadena.toLowerCase();
        cadena = Normalizer
                .normalize(cadena, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
        String cadenaAuxiliar = "";
        resultado = 1;
        for (int i = 0; i < markerList.size(); i++) {
            cadenaAuxiliar = markerList.get(i).getTitle().toLowerCase();
            cadenaAuxiliar = Normalizer
                    .normalize(cadenaAuxiliar, Normalizer.Form.NFD)
                    .replaceAll("[^\\p{ASCII}]", "");
            if (cadena.contains(cadenaAuxiliar)) {
                resultado = 2;
                markerID = i;
                break;
            }
        }
        return resultado;
    }

    public Marker getMarker() {
        return markerList.get(markerID);
    }

    public int getMarkerID() {
        return markerID;
    }*/

}
